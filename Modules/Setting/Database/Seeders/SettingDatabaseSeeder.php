<?php

namespace Modules\Setting\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Setting\Entities\Setting;

class SettingDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::create([
            'company_name' => 'Inbi Nusantara',
            'company_email' => 'admin@inbinusantara.com',
            'company_phone' => '03613206099',
            'notification_email' => 'notification@inbinusantara.com',
            'default_currency_id' => 1,
            'default_currency_position' => 'prefix',
            'footer_text' => 'Inbi Nusantara © 2021',
            'company_address' => 'Banjar, Jl. Dukuh, Bunutin, Kec. Bangli, Kabupaten Bangli, Bali 80614'
        ]);
    }
}
