<a href="{{ route('product-categories.edit', $data->cat_id) }}" class="btn btn-info btn-sm">
    <i class="bi bi-pencil"></i>
</a>
<button id="delete" class="btn btn-danger btn-sm" onclick="
    event.preventDefault();
    if (confirm('Are you sure? It will delete the data permanently!')) {
        document.getElementById('destroy{{ $data->cat_id }}').submit();
    }
    ">
    <i class="bi bi-trash"></i>
    <form id="destroy{{ $data->cat_id }}" class="d-none" action="{{ route('product-categories.destroy', $data->cat_id) }}" method="POST">
        @csrf
        @method('delete')
    </form>
</button>
