<a href="{{ route('product-ingredient.edit', $data->ing_id) }}" class="btn btn-info btn-sm">
    <i class="bi bi-pencil"></i>
</a>
<button id="delete" class="btn btn-danger btn-sm" onclick="
    event.preventDefault();
    if (confirm('Are you sure? It will delete the data permanently!')) {
        document.getElementById('destroy{{ $data->ing_id }}').submit();
    }
    ">
    <i class="bi bi-trash"></i>
    <form id="destroy{{ $data->ing_id }}" class="d-none" action="{{ route('product-ingredient.destroy', $data->ing_id) }}" method="POST">
        @csrf
        @method('delete')
    </form>
</button>
