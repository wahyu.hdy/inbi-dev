@extends('layouts.app')

@section('title', 'Edit Product Ingredient')

@section('breadcrumb')
    <ol class="breadcrumb border-0 m-0">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('products.index') }}">Products</a></li>
        <li class="breadcrumb-item"><a href="{{ route('product-ingredient.index') }}">Ingredient</a></li>
        <li class="breadcrumb-item active">Edit</li>
    </ol>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-7">
                @include('utils.alerts')
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('product-ingredient.update', $ingredient->ing_id) }}" method="POST">
                            @csrf
                            @method('patch')
                            <div class="form-group">
                                <label class="font-weight-bold" for="ingredient_code">Ingredient Code <span class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="ingredient_code" required value="{{ $ingredient->ingredient_code }}">
                            </div>
                            <div class="form-group">
                                <label class="font-weight-bold" for="ingredient_name">Ingredient Name <span class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="ingredient_name" required value="{{ $ingredient->ingredient_name }}">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Update <i class="bi bi-check"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

