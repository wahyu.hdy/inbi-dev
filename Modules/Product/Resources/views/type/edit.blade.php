@extends('layouts.app')

@section('title', 'Edit Product Type')

@section('breadcrumb')
<ol class="breadcrumb border-0 m-0">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route('products.index') }}">Products</a></li>
    <li class="breadcrumb-item"><a href="{{ route('product-type.index') }}">Type</a></li>
    <li class="breadcrumb-item active">Edit</li>
</ol>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-7">
            @include('utils.alerts')
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('product-type.update', $type->jenis_typeid) }}" method="POST">
                        @csrf
                        @method('patch')
                        <div class="form-group">
                            <label class="font-weight-bold" for="jenis_code">Jenis Code <span
                                    class="text-danger">*</span></label>
                            <input class="form-control" type="text" name="jenis_code" required
                                value="{{ $type->jenis_code }}">
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold" for="jenis_name">Jenis Name <span
                                    class="text-danger">*</span></label>
                            <input class="form-control" type="text" name="jenis_name" required
                                value="{{ $type->jenis_name }}">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Update <i class="bi bi-check"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection