<a href="{{ route('product-type.edit', $data->jenis_typeid) }}" class="btn btn-info btn-sm">
    <i class="bi bi-pencil"></i>
</a>
<button id="delete" class="btn btn-danger btn-sm" onclick="
    event.preventDefault();
    if (confirm('Are you sure? It will delete the data permanently!')) {
        document.getElementById('destroy{{ $data->jenis_typeid }}').submit();
    }
    ">
    <i class="bi bi-trash"></i>
    <form id="destroy{{ $data->jenis_typeid }}" class="d-none" action="{{ route('product-type.destroy', $data->jenis_typeid) }}"
        method="POST">
        @csrf
        @method('delete')
    </form>
</button>