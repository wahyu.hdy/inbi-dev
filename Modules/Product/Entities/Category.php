<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $primaryKey = 'cat_id';

    public function products() {
        return $this->belongsTo(Product::class, 'cat_id', 'category_id');
    }
}
