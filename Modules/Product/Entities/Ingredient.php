<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Ingredient extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $primaryKey = 'ing_id';

    public function products() {
        return $this->hasMany(Product::class, 'ingredient_id', 'ing_id');
    }
}
