<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Type extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $primaryKey = 'jenis_typeid';

    public function products() {
        return $this->hasMany(Product::class, 'jenis_id', 'jenis_typeid');
    }
}
