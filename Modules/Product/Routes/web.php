<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => 'auth'], function () {
    //Print Barcode
    Route::get('/products/print-barcode', 'BarcodeController@printBarcode')->name('barcode.print');
    //Product
    Route::resource('products', 'ProductController');
    Route::get('/products-bundle/{id}', 'ProductController@bundle')->name('products-bundle.setbundle');
    Route::post('/products/storebundle', 'ProductController@storebundle')->name('products.storebundle');
    //Product Category
    Route::resource('product-categories', 'CategoriesController')->except('create', 'show');
    Route::resource('product-ingredient', 'IngredientsController')->except('create', 'show');
    Route::resource('product-type', 'TypeController')->except('create', 'show');
});