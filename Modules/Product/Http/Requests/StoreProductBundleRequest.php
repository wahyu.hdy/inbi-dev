<?php

namespace Modules\Product\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class StoreProductBundleRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'product_name' => ['required', 'string', 'max:255'],
            //'product_quantity' => ['required', 'integer', 'min:1'],
            //'product_quantity' => ['required', 'integer', 'min:1'],
            //'product_price' => ['required', 'numeric', 'max:2147483647']
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('create_products');
    }
}
