<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;
use Modules\Product\Entities\Category;
use Modules\Product\Entities\Ingredient;
use Modules\Product\Entities\Product;
use Modules\Product\DataTables\ProductIngredientsDataTable;

class IngredientsController extends Controller
{

    public function index(ProductIngredientsDataTable $dataTable) {
        //abort_if(Gate::denies('access_product_ingredient'), 403);

        return $dataTable->render('product::ingredient.index');
    }


    public function store(Request $request) {
        //abort_if(Gate::denies('access_product_ingredient'), 403);

        $request->validate([
            'ingredient_code' => 'required|unique:ingredients,ingredient_code',
            'ingredient_name' => 'required'
        ]);

        Ingredient::create([
            'ingredient_code' => $request->ingredient_code,
            'ingredient_name' => $request->ingredient_name,
        ]);

        toast('Product Ingredient Created!', 'success');

        return redirect()->back();
    }


    public function edit($id) {
        //abort_if(Gate::denies('access_product_ingredient'), 403);

        $ingredient = Ingredient::findOrFail($id);

        return view('product::ingredient.edit', compact('ingredient'));
    }


    public function update(Request $request, $id) {
        //abort_if(Gate::denies('access_product_ingredient'), 403);

        /*$request->validate([
            'ingredient_code' => 'required|unique:ingredients,ingredient_code,' . $id,
            'ingredient_name' => 'required'
        ]);*/

        Ingredient::findOrFail($id)->update([
            'ingredient_code' => $request->ingredient_code,
            'ingredient_name' => $request->ingredient_name,
        ]);

        toast('Product Ingredient Updated!', 'info');

        return redirect()->route('product-ingredient.index');
    }


    public function destroy($id) {
        //abort_if(Gate::denies('access_product_ingredient'), 403);

        $ingredient = Ingredient::findOrFail($id);
        $product = Product::where('ingredient_id', $id)->first();

        if(!empty($product->id)) {
            return back()->withErrors('Can\'t delete beacuse there are products associated with this ingredient.');
        }

        $ingredient->delete();

        toast('Product Ingredient Deleted!', 'warning');

        return redirect()->route('product-ingredient.index');
    }
}
