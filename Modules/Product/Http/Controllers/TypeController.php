<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;
use Modules\Product\Entities\Category;
use Modules\Product\Entities\Type;
use Modules\Product\Entities\Product;
use Modules\Product\DataTables\ProductTypeDataTable;

class TypeController extends Controller
{

    public function index(ProductTypeDataTable $dataTable) {
        //abort_if(Gate::denies('access_product_type'), 403);

        return $dataTable->render('product::type.index');
    }


    public function store(Request $request) {
        //abort_if(Gate::denies('access_product_type'), 403);

        $request->validate([
            'jenis_code' => 'required|unique:types,jenis_code',
            'jenis_name' => 'required'
        ]);

        Type::create([
            'jenis_code' => $request->jenis_code,
            'jenis_name' => $request->jenis_name,
        ]);

        toast('Product Type Created!', 'success');

        return redirect()->back();
    }


    public function edit($id) {
        //abort_if(Gate::denies('access_product_type'), 403);

        $type = Type::findOrFail($id);

        return view('product::type.edit', compact('type'));
    }


    public function update(Request $request, $id) {
        //abort_if(Gate::denies('access_product_type'), 403);

        /*$request->validate([
            'jenis_code' => 'required|unique:types,jenis_code,' . $id,
            'jenis_name' => 'required'
        ]);*/

        Type::findOrFail($id)->update([
            'jenis_code' => $request->jenis_code,
            'jenis_name' => $request->jenis_name,
        ]);

        toast('Product Type Updated!', 'info');

        return redirect()->route('product-type.index');
    }


    public function destroy($id) {
        //abort_if(Gate::denies('access_product_type'), 403);

        $type = Type::findOrFail($id);
        $product = Product::where('jenis_id', $id)->first();

        if(!empty($product->id)){
            return back()->withErrors('Can\'t delete beacuse there are products associated with this type.');
        }

        $type->delete();

        toast('Product Type Deleted!', 'warning');

        return redirect()->route('product-type.index');
    }
}