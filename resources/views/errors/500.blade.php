@extends('errors.illustrated-layout')

@section('code', '500')

@section('title', __('Server Error'))

@section('message', __('Please Contact Server Administrator'))
