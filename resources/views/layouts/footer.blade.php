<footer class="c-footer">
    <div>{!! settings()->footer_text !!}</div>

    <div class="mfs-auto d-md-down-none">&nbsp;</div>
</footer>
